# Créer un container Docker
```
docker run -d --name composer -p 9898:80 php:8.0-apache
```
```
docker exec -ti composer bash
```

# Installation de composer
créer un ficher composer.sh et suivre les instructions ci-dessous

### composer.sh

```
#!/bin/sh
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer
```

### Commandes
executer les commandes suivantes dans le terminal

```
chmod 777 composer.sh
```

```
./composer.sh
```

```
composer init
```


## Autoload
executer cette commande dans le terminal à chaque modif
```
composer dumpautoload
```
