<?php

namespace Beweb\Td\Models;

abstract class Job {

   abstract function getModifPv(): int;
   abstract function getModifDef(): int;
   abstract function getModifAttack(): int;

}