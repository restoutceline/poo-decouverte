<?php

namespace Beweb\Td\Models\Job\Implementation;

use Beweb\Td\Models\Job;

class Warlock extends Job{
    function getModifPv(): int {
        return 150;
    }

    function getModifAttack(): int{
        return 20;
    }
    function getModifDef(): int{
        return 20;
    }
}
