<?php

namespace Beweb\Td\Models\Race\Implementation;

use Beweb\Td\Models\Race;
use Beweb\Td\Models\Stats;

class Alf extends Race{

    
    
    function __construct(){
        parent::__construct(); //recup des stats vides de Race, fait un new stat et stocke dans $modifiers
        $this->modifiers->pv = 50; 
        $this->modifiers->attack = 2;
        $this->modifiers->def = 25;
    }
}

