<?php

namespace Beweb\Td\Models;

/**
 * les stats regroupent les pv def et attack
 * pv = point de vie
 * def = défense
 * attack = attaque
 */

class Stats{
    public int $pv;
    public int $def;
    public int $attack;

    function __construct(){
        $this->pv =0;
        $this->fef =0;
        $this->attack = 0;

    }
}